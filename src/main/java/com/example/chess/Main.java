package com.example.chess;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Starting App...");
        logger.debug("Loading properties");
        Properties appProperties = PropertyLoader.loadFile("app.properties");

        logger.debug("Validating properties");
        PropertyValidator propertyValidator = new PropertyValidator(appProperties);
        propertyValidator.validate();

        CommonResource matrix = new CommonResource(Integer.parseInt(appProperties.getProperty("matrixSize")));

        int threadNumber = Integer.parseInt(appProperties.getProperty("threadNumber"));
        ArrayList<Thread> threads = new ArrayList<>();
        for (int i = 0; i < threadNumber; i++) {
            threads.add(new DiagonalThread(i + 1, matrix));
            threads.get(i).start();
        }

        try {
            TimeUnit.SECONDS.sleep(1);
            for (Thread t: threads) {
                TimeUnit.SECONDS.timedJoin(t, 10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logger.info("\n" + matrix);
        logger.info("End of the main thread.");
    }
}
