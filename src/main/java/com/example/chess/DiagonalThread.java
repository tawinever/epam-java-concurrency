package com.example.chess;

public class DiagonalThread extends Thread {
    private int threadId;
    private CommonResource commonResource;

    public DiagonalThread(int threadId, CommonResource commonResource) {
        this.threadId = threadId;
        this.commonResource = commonResource;
    }

    @Override
    public void run() {
        commonResource.changeDiagonal(threadId);
    }
}
