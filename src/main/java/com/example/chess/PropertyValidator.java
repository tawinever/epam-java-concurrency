package com.example.chess;

import java.util.Properties;

public class PropertyValidator {
    private Properties properties;

    public PropertyValidator(Properties properties) {
        this.properties = properties;
    }

    public void validate() {
        validateInteger("matrixSize", 8, 12);
        validateInteger("threadNumber", 4, 6);
    }

    protected void validateInteger(String key, int minValue, int maxValue) {
        int val = Integer.parseInt(properties.getProperty(key));
        if (val < minValue || val > maxValue) {
            throw new IllegalArgumentException("Illegal value: " + key);
        }
    }
}
