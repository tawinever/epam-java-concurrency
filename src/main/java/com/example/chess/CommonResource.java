package com.example.chess;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class CommonResource {
    private static final Logger logger = LogManager.getLogger(Main.class);

    int currentDiagonal;
    int size;
    int[][] grid;
    ReentrantLock mutex = new ReentrantLock();

    public CommonResource(int size) {
        this.size = size;
        currentDiagonal = 0;
        grid = new int[this.size][this.size];
        for (int i = 0; i < this.size; i++) {
            grid[i][i] = 0;
        }
    }

    public void changeDiagonal(int threadValue) {
        while(currentDiagonal < size) {
            mutex.lock();
            try {
                logger.info(threadValue + " Thread changing diagonal " + currentDiagonal + " to " + threadValue);
                grid[currentDiagonal][currentDiagonal] = threadValue;
                currentDiagonal++;
            } finally {
                mutex.unlock();
            }

            try {
                TimeUnit.MILLISECONDS.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                stringBuilder.append(grid[i][j] + " ");
            }
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }
}
