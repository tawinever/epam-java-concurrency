package com.example.chess;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {
    public static Properties loadFile(String fileName) {
        Properties properties = new Properties();
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String appFullPath = rootPath + fileName;

        try {
            properties.load(new FileInputStream(appFullPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties;
    }
}
